/*
	Name:Jackpot game v3.2.1.9.3 
	Author: Aryan
	Date: 28-07-13 12:10
	Description: This is a game in which you which means the user has to guess the correct number
*/
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void Start();
void GetResults();

int i, j, life, maxrand;
char c;
char name[100];

void Start() {
	i = 0;
	j = 0;
	life = 0;
	maxrand = 6;
	
	cout <<"What is your name? \n ";

    cin >>name;
	cout << "Select difficulty mode:\n"; // the user has to select a difficutly level
	cout << "1 : very easy(0-50)\n";
	cout << "2 : Easy (0-100)\n";
	cout << "3 : Medium (0-500)\n";
	cout << "4 : Difficult (0-1000)\n";
	cout << "5 : MOST Difficult (0-10000)\n";
	cout << "or type another key to quit\n";
	c = 30;

	cin >> c;                   // read the user's choice
	cout << "\n";

	switch (c) {
		case '1':
			maxrand = 50;  // the random number will be between 0 and maxrand
			break;
		case '2':
			maxrand = 100;
			break;
		case '3':
			maxrand = 500;
	
		case '4':
			maxrand= 1000;
			break;
			case '5':
			maxrand= 10000;
			break;
		
		default:
			exit(0);
		break;
	}

	life = 15;         // number of lifes of the player
	srand((unsigned)time(NULL)); // init Rand() function
	j = rand() % maxrand;  // j get a random value between 0 and maxrand
	
	GetResults();
}

void GetResults() {
	if (life <= 0) { // if player has no more life then he loses
		cout << "You lose !Try Again!\n\n";
		cout << "please wait...................loading.............!Done!\n";
		Start();
	}

	cout << "Type a number: \n";
	cin >> i;
	
	if((i>maxrand) || (i<0)) { // if the user number isn't correct, restart
		cout << "Error: number not between 0 and \n" << maxrand;
		GetResults();
	}

	if(i == j) {
		cout << "***Congratulatulaions*** YOU WIN!\n\n"; // the user found the secret number
		cout << "please wait .......... loading.............! Done! Thanks for playing\n"; 
		Start();
	} else if(i>j) {
		cout << " BIGGER THAN THE ACTUAL NUMBER\n";
		life = life - 0;
		cout << "Lives remaining: " << life << "\n\n";
		GetResults();
	} else if(i<j) {
		cout << " SMALLER THAN THE ACTUAL NUMBER\n";
		life = life - 1;
		cout << "Lives remaining: " << life << "\n\n";
		GetResults();
	}
}

int main() {
	cout << "** Jackpot game ** v.3.2.1.9.3\n";
	cout << "The goal of this game is to guess a number.\n";
	cout << "Jackpot will tell you if the number is bigger than the actual number or\n";
	cout << "smaller compared to the secret number to find.\n\n";
	cout << "You have 10 lives to play.\n";
	cout << "Smaller than the actual number = life - 1 & bigger = life - 0.\n";
	cout << "You need type the key in `Select Difficluty'.\n";
	Start();
	return 0;
}

