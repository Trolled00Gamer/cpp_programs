// Make a calculator using classes

#include "pch.h"
#include "iostream"

class calc {
	int x, y;
public:
	calc() {
		x = 0;
		y = 0;
		std::cout << "Welcome! Enter number correponding to your the operation you want done (only for 2 values): \n";
		std::cout << "\n1. Addition (Find the sum of 2 numbers)";
		std::cout << "\n2. Subtraction (Find the dfference between first and the second value)";
		std::cout << "\n3. Multiplication (Find the product of 2 values)";
		std::cout << "\n4. Division (Find the quotient when dividing the second value from the first value)";
		std::cout << "\n5. Mod (Find the absolute of both the values)";
		std::cout << "\n6. Find whether odd or even (for both values)\n";
		choice();
	}
	void choice();	//to enter the choice and be direct to the appropriate function
	void get_values(); //to get the values
	void add();		//to add the values, duh
	void subtract(); //to find the difference between the values, srsly?
	void multiply(); // to find the product of 2 values, why are you even reading these?
	void division(); //to find the quotient when dividing the second value by the first value, don't you already know what division means? 
	void mod();	//to find the absolute of the 2 values, you were supposed to know these from as early as 2nd standard
	void odd_eve();	//to find the whether the values entered are odd or even, *facepalms*
};
void calc::choice() {
	char ch;
	std::cout << "\nEnter your choice:\t";
	std::cin >> ch;
	switch (ch)
	{
	case '1':
		get_values();
		add();
		break;
	case '2':
		get_values();
		subtract();
		break;
	case '3':
		get_values();
		multiply();
		break;
	case '4':
		get_values();
		division();
		break;
	case '5':
		get_values();
		mod();
		break;
	case '6':
		get_values();
		odd_eve();
		break;
	default:
		break;
	}
}
void calc::get_values() {
	std::cout << "\nEnter the first value:\t";
	std::cin >> x;
	std::cout << "\nEnter the second value:\t";
	std::cin >> y;
}
void calc::add() {
	int sum = x + y;
	std::cout << "\nThe sum is\t" << sum;
}
void calc::subtract() {
	int sum = x - y;
	std::cout << "\nThe between the first and second value is\t" << sum;
}
void calc::multiply() {
	int sum = x * y;
	std::cout << "\nThe product is\t" << sum;
}
void calc::division() {
	int sum = x / y;
	std::cout << "\nThe quotient is\t" << sum;
}
void calc::mod() {
	int z_F = abs(x);
	int z_S = abs(y);
	std::cout << "\nThe mod of the first value is:\t" << z_F << "\nThe mod of the second value is:\t" << z_S;
}
void calc::odd_eve() {
	if (x % 2 == 0)
		std::cout << "\nThe first value is even\t";
	else
		std::cout << "\nThe first value is odd\t";
	if (y % 2 == 0)
		std::cout << "\nThe second value is even\t";
	else
		std::cout << "\nThe second value is odd\t";
}

int main() {  //This is the 'main' function. Program execution begins and ends here.
	calc c;	//Mostly, the whole program runs using a constructor
	std::cout << "\n";
	return 0; 
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
