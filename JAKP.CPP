/*
	Name:Jackpot game v5
	Author: Aryan G
	Date: 16-12-17 14:50
	Description: This is a game in which you which means the user has to guess the correct number
*/
#include <iostream.h>
#include <stdlib.h>
#include <time.h>
#include <fstream.h>
#include <conio.h>
#include <process.h>
#include <stdio.h>
class Menu
{
	public :
	void Main_Menu();
	private :
	void Game_Menu();
	void About();
	void Modify_levels();
	void View_Scores();
};
class Score
{
	public :
	void scores_read();
	void scores_write();
};
class Game
{
	public :
	void level(int , int );
	private :
	int i, j, life, maxrand, hint;
	char c;
	char name[20];
};

void Menu :: Main_Menu () {
	char ch;
	gotoxy(29,6);
	cout << "M A I N   M E N U \n";
	gotoxy(30,10);
	cout << "1: START GAME \n";
	gotoxy(30,11);
	cout << "2: ABOUT  \n";
	gotoxy(30,12);
	cout << "3: MY SCORES  \n ";
	gotoxy(30,13);
	cout << "0: QUIT \n";
	gotoxy(30,14);
	cout << "ENTER YOUR CHOICE :  \n";
	gotoxy(30,15);
	cin>>ch;
	switch(ch)
	{
	case '1' :
	Game_Menu();
	break;
	case '2' :
	About();
	break;
	case '3':

	break;
	default :
	exit(0);
	}
}
void Menu :: Game_Menu () {
	clrscr();
	gotoxy(10,5);
	cout << "The goal of this game is to guess a number.\n";
	char c;
	Game G;
	gotoxy(10,7);
	cout << "Alrite.So, which difficulty do want to try today????? \n"; // the user has to select a difficutly level
	gotoxy(10,8);
	cout << "1 : Easy(0-50) \n";
	gotoxy(10,9);
	cout << "2 : Normal (0-100)\n";
	gotoxy(10,10);
	cout << "3 : Hard (0-500)\n";
	gotoxy(10,11);
	cout << "4 : Mythic (0-100)*\n";
	gotoxy(10,12);
	cout << "or type another key to quit\n";

	cin >> c;                   // read the user's choice
	cout << "\n";

	switch (c) {
		case '1':
			G.level(51,15);
			break;
		case '2':
			G.level(101,15);
			break;
		case '3':
			G.level(501,15);
			break;
		case '4':
			G.level(101,5);
			break;
		default:
			exit(0);
		break;
	}

	 // number of lifes of the player
	// init Rand() function
       // j get a random value between 0 and maxrand
}

void Menu :: About()
{
	clrscr();
	cout <<"** Jackpot Game ** \n";
	cout <<" Project By Aryan G XIIth - A\n";
	cout <<" Made with the help of Turbo C++ , Notepad , Wordpad and reference books \n";
	cout <<" Hope you enjoy(ed) \n";
	Main_Menu();
}
void Menu :: View_Scores()
{
	int a=10;
	char c[10];
	clrscr();
	ifstream f;
	f.open ("SCORES.DAT" , ios :: in | ios::binary);
	{
	cout << " Score in Easy Difficulty \t";
	cout << " Score Medium Difficulty \t";
	cout << " Score Medium Difficulty \t";
	cout << " Score Medium Difficulty \t";
	}
	f.close();
}

void Game :: level(int maxrand, int life = 15)
{
	char c;
	Score S;
	Menu M;
	srand((unsigned)time(NULL));
	j = rand() % maxrand;
	cout << " The randomly generated number can have a max value of"<< maxrand-1 <<" \n"
	     << " You have " << life << " lives at the start of the game \n"
	     << " If the number you guessed is higher or smaller than the randomly generated number \n"
	     << " Your life gets decreased by 1 \n"
	     << " Let's start! : \n"
	     << " Enter a number between 0 and "<< maxrand -1 <<"  \t";
	if (life <= 0) { // if player has no more life then he loses
		clrscr();
		gotoxy(30,15);
		cout << "You lose !Better Luck next time! \n\n";
		 cin >> c;
		M.Main_Menu();
	}

	a:cout << "Type a number: \n";
	cin >> i;

	if((i>maxrand-1) || (i<0)) { // if the user number isn't correct, restart

	cout << "The number you entered is either greater than " <<maxrand<<" or less"
	     << "than 0";
	}
	if(i == j) {
	clrscr();
	gotoxy(30,5);
	cout << "***Congratulatulaions*** ! YOU WON! \n\n"; // the user found the secret number
		cout << " Thanks for playing\n";

		M.Main_Menu();
	}
	 else if(i>j) {
		cout << "The entered number in greater than the actual number\n";
		life = life - 1;
		cout << "Lives remaining: " << life << "\n\n";
		goto a;
	}
	else if(i<j) {
		cout << "The entered number in less than the actual number\n";
		life = life - 1;
		cout << "Lives remaining: " << life << "\n\n";
		goto a;
	}

}
void Score :: scores_write ()
{
	fstream f;
	f.open("SCORES.DAT", ios :: out | ios :: binary );
	int c=10;
	while (!f.eof())
	{
	f.write((char *)&c,sizeof(c));
	}
	f.close();
}

void Score :: scores_read ()
{
	fstream f;
	f.open("SCORES.DAT", ios :: in | ios :: binary );
	char c;
	while (!f.eof())
	{
	f.read(&c, sizeof(c));
	cout<<c;
	}
	f.close();

}
void main() {
	clrscr();
	Score s;
	s.scores_write();
	s.scores_read();
	Menu M;
	gotoxy(18,3);
	cout << "C++ Project on the ** Jackpot game ** v5 \n";
	M.Main_Menu();
}